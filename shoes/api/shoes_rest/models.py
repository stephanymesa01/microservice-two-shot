from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)
    def __str__(self):
        return self.closet_name


class Shoe(models.Model):

    bin = models.ForeignKey(
        BinVO, 
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    manufacturer = models.CharField(max_length=100)
    picture_url = models.URLField()
    def __str__(self):
        #return f"{self.model_name} {self.color} {self.manufacturer} {self.picture_url} at {self.bin.closet_name}"
        return self.bin.closet_name