# Wardrobify

**TEAM |**

* Person 1 - Stephany is responsible for the Hats Microservice.
* Person 2 - Jose is responsible for the Shoes Microservice.

## Design

[Link to Screenshot](file://localhost/Users/steph/Desktop/Screenshot%202024-03-12%20at%2010.39.54%E2%80%AFAM.png)

## Shoes microservice
Explain your models and integration with the wardrobe
microservice, here.

Our Shoes microservice has two Models:
    The Shoe Model with the model_name(Char), color(Char), manufacturer(Char), picture_url(URL) and bin(Foreign Key). 
    The "bin" Foreign Key relates to the Project "Bin" Model with properties closet_name (Char), bin_number(Int), bin_size(Int).

    The Warodrobe Bin Model provides a way to store shoes in a closets and bins according to their closet name and bin number, respectively.

    The two models provided the funtionality for a user to be able to add Shoe instances with the above properties and the functionality to create, edit and delete on those properties.

    Using HTMX/JS/Bootstrap/React we are able to create cards as the main components for the user interaction. These tools also provide methods to validate user input before saving to the database as well as providing Error Handling messages if applicable.


## Hats microservice

**Integration with the Wardrobe Microservice** |
<details><summary>Models</summary>

In our Hats microservice, we have two main types of information in our Models.py file:


**Location Info:** This tells us where hats are stored in the wardrobe, like which closet, section, and shelf.


**Hat Details:** This includes info about each hat, such as what it's made of, its style, color, and even a picture.
</details>


**How Hats Microservice Works with Wardrobe Microservice |**

Our microservice checks with the Wardrobe microservice to see where each hat is in the wardrobe. If someone adds a new hat, our microservice makes sure the location they say it's in is valid in the wardrobe.

**Viewing, Updating, and Removing Hats |**

If someone wants to see details about a specific hat, change something about it, or remove it, they use our microservice.

Our microservice finds the hat they're asking about and shows its details. If they want to change something, we check that the new info is okay before making the change. And if they want to remove a hat, we also take it out of the wardrobe.

**Summary |**

Our Hats microservice works closely with the Wardrobe microservice to keep track of where hats are stored. By ensuring we have the right details and talking to the Wardrobe microservice when needed, we help keep everything organized so everyone can easily find their hats.
