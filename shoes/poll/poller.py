import django
import os
import sys
import time
import json
import requests
import django

# Add the directory containing the Django project to Python path
sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import Shoe model after setting up Django environment

from shoes_rest.models import BinVO



def get_bins():
    # Fetch shoe data from the Wardrobe API
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    return content["bins"]


def update_or_create_bin(bin):
    BinVO.objects.update_or_create(
        import_href=bin["href"],
        defaults={
            "closet_name": bin["closet_name"],
            "bin_number": bin["bin_number"],
            "bin_size": bin["bin_size"],
        }
    )


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            bins = get_bins()       # Get shoe data from Wardrobe API
            for bin_data in bins:
                update_or_create_bin(bin_data) 
        except Exception as e:      # Handle exceptions
            print(e, file=sys.stderr)
        time.sleep(60)              # Wait for 60 seconds before polling again


if __name__ == "__main__":
    poll()
