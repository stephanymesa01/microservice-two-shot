from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoe
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href", 
        "closet_name", 
        "bin_number",
        "bin_size", 
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id", 
        "model_name", 
        "color", 
        "manufacturer", 
        "picture_url",
        "bin",
    ]
    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, shoe_id=None):
    if request.method == "GET":
        shoes = Shoe.objects.all().order_by("id")
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )

    elif request.method == "POST":
        print(True)
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(shoes, encoder=ShoeListEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            if "bin" in content:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Location"})
        Shoe.objects.filter(id=pk).update(**content)
        hat = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )
    
