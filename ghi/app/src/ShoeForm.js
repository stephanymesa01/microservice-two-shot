import React, { useEffect, useState } from "react";

function ShoeForm() {
  const [bins, setBins] = useState([]);
  const [formData, setFormData] = useState({
    model_name: "",
    color: "",
    manufacturer: "",
    picture_url: "",
    bin: ""
  });

  const handleFormChange = event => {
    const { value, name } = event.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = async event => {
    event.preventDefault();

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json"
      }
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log("NEW SHOE CREATED:", newShoe);
      setFormData({
        model_name: "",
        color: "",
        manufacturer: "",
        picture_url: "",
        bin: ""
      });
    } else {
      console.error("CREATE NEW SHOE, FAILED:", response.status);
    }
  };

  const fetchData = async () => {
    const binUrl = "http://localhost:8100/api/bins/";
    const response = await fetch(binUrl);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    } else {
      console.error("Bad Response: ", response.status);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a NEW shoe!</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input
                value={formData.model_name}
                onChange={handleFormChange}
                name="model_name"
                placeholder="Shoe Model"
                type="text"
                id="model_name"
                className="form-control"
                required
              />
              <label htmlFor="model_name">Shoe Model</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.color}
                onChange={handleFormChange}
                name="color"
                placeholder="Color"
                type="text"
                id="color"
                className="form-control"
                required
              />
              <label htmlFor="color">Shoe Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.manufacturer}
                onChange={handleFormChange}
                name="manufacturer"
                placeholder="Manufacturer"
                type="text"
                id="manufacturer"
                className="form-control"
                required
              />
              <label htmlFor="manufacturer">Shoe Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.picture_url}
                onChange={handleFormChange}
                name="picture_url"
                placeholder="picture_url"
                type="url"
                id="picture_url"
                className="form-control"
                required
              />
              <label htmlFor="picture_url">Shoe Photo URL</label>
            </div>
            <div className="mb-3">
              <select
                value={formData.bin}
                onChange={handleFormChange}
                name="bin"
                id="bin"
                className="form-select"
                required
              >
                <option value="">Choose a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.href}>
                      {bin.id}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;