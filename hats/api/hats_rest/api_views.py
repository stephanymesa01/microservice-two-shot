from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .models import LocationVO, Hat
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
        "id",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all().order_by("id")
        return JsonResponse({"hats": hats}, encoder=HatListEncoder, safe=False)
    elif request.method == "POST":
        content = json.loads(request.body)
        if "location" not in content:
            return JsonResponse(
                {"message": "Missing 'location' in request body"}, status=400
            )

        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)

        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatListEncoder, safe=False)


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, id):
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Location"})
        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )
