from django.urls import path
from .api_views import api_list_shoes, api_show_shoe


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_show_shoe, name="api_show_shoe"),

    # path(
    #     "conferences/<int:conference_vo_id>/attendees/",
    #     api_list_attendees,
    #     name="api_list_attendees",
    # ),
    # path("attendees/<int:pk>/", api_show_attendee, name="api_show_attendee"),
]
