import React from "react";
import { NavLink } from "react-router-dom";
import "./index.css";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          Wardrobify
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownShoe"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Shoes
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownShoe"
              >
                <li>
                  <NavLink className="dropdown-item" to="/shoes">
                    All Shoes
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/shoes/new">
                    Create Shoe
                  </NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownHat"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Hats
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownHat">
                <li>
                  <NavLink className="dropdown-item" to="/hats">
                    All Hats
                  </NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item" to="/hats/new">
                    New Hat
                  </NavLink>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
