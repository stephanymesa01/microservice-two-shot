import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatList from "./HatList";
import NewHatForm from "./NewHatForm";
import ShoeList from "./ShoeList";
import ShoeForm from "./ShoeForm"; 
import Footer from "./Footer";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/">
            <Route index element={<HatList />} />
            <Route path="new/" element={<NewHatForm />} />
          </Route>
          <Route path="shoes/">
            <Route index element={<ShoeList />} />
            <Route path="new/" element={<ShoeForm />} />
          </Route>
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
