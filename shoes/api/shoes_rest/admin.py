from django.contrib import admin
from .models import BinVO, Shoe


@admin.register(BinVO)
class BinAdmin(admin.ModelAdmin):
    list_display = [
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",
    ]


@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = [
        "model_name",
        "color",
        "manufacturer",
        "picture_url",
        "bin",
    ]