import React, { useEffect, useState } from "react";
import "./index.css";


const deleteHat = async (id, setHatColumns, hatColumns) => {
  const deleteUrl = `http://localhost:8090/api/hats/${id}/`;
  const fetchOptions = {
    method: "DELETE"
  };

  try {
    const response = await fetch(deleteUrl, fetchOptions);
    if (response.ok) {

      const updatedHatColumns = hatColumns.map(column =>
        column.filter(hat => hat.id !== id)
      );
      setHatColumns(updatedHatColumns);
    } else {
      console.log("Failed to delete hat:", response.statusText);
    }
  } catch (error) {
    console.error("Error deleting hat:", error);
  }
};

const updateHat = async (id, updatedAttributes, setHatColumns, hatColumns) => {
  const updateUrl = `http://localhost:8090/api/hats/${id}/`;
  const fetchOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(updatedAttributes)
  };

  try {
    const response = await fetch(updateUrl, fetchOptions);
    if (response.ok) {

      const updatedHatColumns = hatColumns.map(column =>
        column.map(hat => (hat.id === id ? { ...hat, ...updatedAttributes } : hat))
      );
      setHatColumns(updatedHatColumns);
    } else {
      console.log("Failed to update hat:", response.statusText);
    }
  } catch (error) {
    console.error("Error updating hat:", error);
  }
};

function HatCard({ hat, setHatColumns, hatColumns }) {
  const [isEditing, setIsEditing] = useState(false);
  const [updatedAttributes, setUpdatedAttributes] = useState({
    fabric: hat.fabric,
    style_name: hat.style_name,
    color: hat.color
  });

  const handleUpdate = () => {
    updateHat(hat.id, updatedAttributes, setHatColumns, hatColumns);
    setIsEditing(false);
  };

  const handleChange = e => {
    const { name, value } = e.target;
    setUpdatedAttributes({
      ...updatedAttributes,
      [name]: value
    });
  };

  const handleEdit = () => {
    setIsEditing(true);
  };

  const handleCancelEdit = () => {
    setIsEditing(false);
  };

  const handleDelete = () => {
    if (window.confirm("Are you sure you want to delete this hat?")) {
      deleteHat(hat.id, setHatColumns, hatColumns);
    }
  };

  return (
    <div key={hat.id} className="card mb-3 shadow p-3">
      <img src={hat.picture_url} className="card-img-top" alt={`Fabric: ${hat.fabric}, style: ${hat.style_name}, color: ${hat.color}`} />
      <div className="card-body">
        {isEditing ? (
          <form>
            <div className="mb-3">
              <label htmlFor="fabric" className="form-label">Fabric</label>
              <input type="text" className="form-control" id="fabric" name="fabric" value={updatedAttributes.fabric} onChange={handleChange} />
            </div>
            <div className="mb-3">
              <label htmlFor="styleName" className="form-label">Style Name</label>
              <input type="text" className="form-control" id="styleName" name="style_name" value={updatedAttributes.style_name} onChange={handleChange} />
            </div>
            <div className="mb-3">
              <label htmlFor="color" className="form-label">Color</label>
              <input type="text" className="form-control" id="color" name="color" value={updatedAttributes.color} onChange={handleChange} />
            </div>
            <button type="button" className="btn btn-primary me-2" onClick={handleUpdate}>Update</button>
            <button type="button" className="btn btn-secondary" onClick={handleCancelEdit}>Cancel</button>
          </form>
        ) : (
          <ul className="list-group list-group-flush">
            <li className="text-capitalize list-group-item">
              <span className="fw-bold">Fabric:</span> {hat.fabric}
            </li>
            <li className="list-group-item">
              <span className="fw-bold">Style:</span> {hat.style_name}
            </li>
            <li className="list-group-item">
              <span className="fw-bold">Color:</span> {hat.color}
            </li>
            <li className="list-group-item">
              <span className="fw-bold">Closet:</span> {hat.location.closet_name}
            </li>
            <li className="list-group-item">
              <span className="fw-bold">Section:</span> {hat.location.section_number}
            </li>
            <li className="list-group-item">
              <span className="fw-bold">Shelf:</span> {hat.location.shelf_number}
            </li>
          </ul>
        )}
      </div>
      <div className="card-footer text-center mb-3 shadow p-3">
        {!isEditing && (
          <>
            <button type="button" className="btn btn-outline-danger me-2" onClick={handleDelete}>Delete</button>
            <button type="button" className="btn btn-outline-secondary" onClick={handleEdit}>Edit</button>
          </>
        )}
      </div>
    </div>
  );
}

function HatColumn({ list, setHatColumns, hatColumns }) {
  return (
    <div className="col">
      {list.map((hat, index) => (
        <HatCard key={index} hat={hat} setHatColumns={setHatColumns} hatColumns={hatColumns} />
      ))}
    </div>
  );
}

const HatList = () => {
  const [hatColumns, setHatColumns] = useState([[], [], []]);
  const enableButtonPointers = true;

  const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const columns = [[], [], []];
        let index = 0;
        for (const hat of data.hats) {
          columns[index].push(hat);
          index = (index + 1) % 3;
        }
        setHatColumns(columns);
      } else {
        console.error("Failed to fetch hats:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching hats:", error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col col-12">
          <div className="card text-center shadow-light mb-3">
            <h2 className="card-title display-4 custom-color">Magical Hats</h2>
          </div>
        </div>
      </div>
      <div className="row">
        {hatColumns.map((hatColumnList, index) => (
          <HatColumn key={index} list={hatColumnList} setHatColumns={setHatColumns} hatColumns={hatColumns} />
        ))}
      </div>
    </div>
  );
};

export default HatList;
