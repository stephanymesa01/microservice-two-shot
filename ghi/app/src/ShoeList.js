import React, { useEffect, useState } from "react";

const deleteShoe = async (id, setShoeColumns, shoeColumns) => {
  const deleteUrl = `http://localhost:8080/api/shoes/${id}/`;
  const fetchOptions = {
    method: "DELETE"
  };

  try {
    const response = await fetch(deleteUrl, fetchOptions);
    if (response.ok) {
      const updatedShoeColumns = shoeColumns.map(column =>
        column.filter(shoe => shoe.id !== id)
      );
      setShoeColumns(updatedShoeColumns);
    } else {
      console.log("Failed to delete shoe:", response.statusText);
    }
  } catch (error) {
    console.error("Error deleting shoe:", error);
  }
};

const updateShoe = async (id, updatedAttributes, setShoeColumns, shoeColumns) => {
  const updateUrl = `http://localhost:8080/api/shoes/${id}/`;
  const fetchOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(updatedAttributes)
  };

  try {
    const response = await fetch(updateUrl, fetchOptions);
    if (response.ok) {
      const updatedShoeColumns = shoeColumns.map(column =>
        column.map(shoe => (shoe.id === id ? { ...shoe, ...updatedAttributes } : shoe))
      );
      setShoeColumns(updatedShoeColumns);
    } else {
      console.log("Failed to update shoe:", response.statusText);
    }
  } catch (error) {
    console.error("Error updating shoe:", error);
  }
};

function ShoeCard({ shoe, setShoeColumns, shoeColumns }) {
  const [isEditing, setIsEditing] = useState(false);
  const [updatedAttributes, setUpdatedAttributes] = useState({
    model_name: shoe.model_name,
    color: shoe.color,
    manufacturer: shoe.manufacturer
  });

  const handleUpdate = () => {
    updateShoe(shoe.id, updatedAttributes, setShoeColumns, shoeColumns);
    setIsEditing(false);
  };

  const handleChange = e => {
    const { name, value } = e.target;
    setUpdatedAttributes({
      ...updatedAttributes,
      [name]: value
    });
  };

  const handleEdit = () => {
    setIsEditing(true);
  };

  const handleCancelEdit = () => {
    setIsEditing(false);
  };

  const handleDelete = () => {
    if (window.confirm("Are you sure you want to delete this shoe?")) {
      deleteShoe(shoe.id, setShoeColumns, shoeColumns);
    }
  };

  return (
    <div key={shoe.id} className="card mb-3 shadow p-3">
      <img src={shoe.picture_url} className="card-img-top" alt={`Model: ${shoe.model_name}, Color: ${shoe.color}, Manufacturer: ${shoe.manufacturer}`} />
      <div className="card-body">
        {isEditing ? (
          <form>
            <div className="mb-3">
              <label htmlFor="modelName" className="form-label">Model Name</label>
              <input type="text" className="form-control" id="modelName" name="model_name" value={updatedAttributes.model_name} onChange={handleChange} />
            </div>
            <div className="mb-3">
              <label htmlFor="color" className="form-label">Color</label>
              <input type="text" className="form-control" id="color" name="color" value={updatedAttributes.color} onChange={handleChange} />
            </div>
            <div className="mb-3">
              <label htmlFor="manufacturer" className="form-label">Manufacturer</label>
              <input type="text" className="form-control" id="manufacturer" name="manufacturer" value={updatedAttributes.manufacturer} onChange={handleChange} />
            </div>
            <button type="button" className="btn btn-primary me-2" onClick={handleUpdate}>Update</button>
            <button type="button" className="btn btn-secondary" onClick={handleCancelEdit}>Cancel</button>
          </form>
        ) : (
          <ul className="list-group list-group-flush">
            <li className="list-group-item">Model: {shoe.model_name}</li>
            <li className="list-group-item">Color: {shoe.color}</li>
            <li className="list-group-item">Manufacturer: {shoe.manufacturer}</li>
          </ul>
        )}
      </div>
      <div className="card-footer text-center">
        {!isEditing && (
          <>
            <button type="button" className="btn btn-outline-danger me-2" onClick={handleDelete}>Delete</button>
            <button type="button" className="btn btn-outline-primary" onClick={handleEdit}>Edit</button>
          </>
        )}
      </div>
    </div>
  );
}

function ShoeColumn({ list, setShoeColumns, shoeColumns }) {
  return (
    <div className="col">
      {list.map((shoe, index) => (
        <ShoeCard key={index} shoe={shoe} setShoeColumns={setShoeColumns} shoeColumns={shoeColumns} />
      ))}
    </div>
  );
}

const ShoeList = () => {
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/";
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const columns = [[], [], []];
        let index = 0;
        for (const shoe of data.shoes) {
          columns[index].push(shoe);
          index = (index + 1) % 3;
        }
        setShoeColumns(columns);
      } else {
        console.error("Failed to fetch shoes:", response.statusText);
      }
    } catch (error) {
      console.error("Error fetching shoes:", error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col col-12">
          <div className="card text-center shadow-light mb-3">
            <h1 className="card-title text-uppercase">Shoes 4 U</h1>
          </div>
        </div>
      </div>
      <div className="row">
        {shoeColumns.map((shoeColumnList, index) => (
          <ShoeColumn key={index} list={shoeColumnList} setShoeColumns={setShoeColumns} shoeColumns={shoeColumns} />
        ))}
      </div>
    </div>
  );
};

export default ShoeList;
