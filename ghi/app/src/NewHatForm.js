import React, { useEffect, useState } from "react";

function NewHatForm() {
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState({
    fabric: "",
    style_name: "",
    color: "",
    picture_url: "",
    location: ""
  });

  const handleFormChange = event => {
    const { value, name } = event.target;
    setFormData({
      ...formData,
      [name]: value
    });
  };

  const handleSubmit = async event => {
    event.preventDefault();

    const hatUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json"
      }
    };

    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log("New hat created:", newHat);
      setFormData({
        fabric: "",
        style_name: "",
        color: "",
        picture_url: "",
        location: ""
      });
    } else {
      console.error("Failed to create hat:", response.status);
    }
  };

  const fetchData = async () => {
    const locationUrl = "http://localhost:8100/api/locations/";
    const response = await fetch(locationUrl);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    } else {
      console.error("Bad Response: ", response.status);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a New Hat!</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                value={formData.fabric}
                onChange={handleFormChange}
                name="fabric"
                placeholder="Fabric"
                type="text"
                id="fabric"
                className="form-control"
                required
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.style_name}
                onChange={handleFormChange}
                name="style_name"
                placeholder="Style Name"
                type="text"
                id="style_name"
                className="form-control"
                required
              />
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.color}
                onChange={handleFormChange}
                name="color"
                placeholder="Color"
                type="text"
                id="color"
                className="form-control"
                required
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.picture_url}
                onChange={handleFormChange}
                name="picture_url"
                placeholder="picture_url"
                type="url"
                id="picture_url"
                className="form-control"
                required
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select
                value={formData.location}
                onChange={handleFormChange}
                name="location"
                id="location"
                className="form-select"
                required
              >
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.href}>
                      {location.id}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NewHatForm;
