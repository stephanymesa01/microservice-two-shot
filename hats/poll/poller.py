import os
import sys
import time
import json
import requests
import django

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()


from hats_rest.models import LocationVO


def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    return content.get("locations")


def update_or_create_locations(location):
    LocationVO.objects.update_or_create(
        import_href=location["href"],
        defaults={
            "closet_name": location["closet_name"],
            "section_number": location["section_number"],
            "shelf_number": location["shelf_number"],
        },
    )


def poll():
    while True:
        print("Hats poller polling for data")
        try:
            locations = get_locations()
            for location_data in locations:
                update_or_create_locations(location_data)
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    # Start polling
    poll()
